require "./level/level.rb"
require "./menu/menu.rb"
require "./colors/color.rb"

class Game
  def initialize
    @board = ["0", "1", "2", "3", "4", "5", "6", "7", "8"]

    @player_one = "X".red
    @player_two = "O".green

    @enter = "E N T R A D A: [0-8]:".magenta
    @end_game = "\nF I M  D E  J O G O\n\n".red

    @total_count

    menu = Menu.new
    menu.menu_players(self)
  end

  def board
    puts "#{@board[0]} | #{@board[1]} | #{@board[2]} \n===+===+===\n#{@board[3]} | #{@board[4]} | #{@board[5]} \n===+===+===\n#{@board[6]} | #{@board[7]} | #{@board[8]} \n\n"
  end

  def player_to_computer(level=0)

    board
    puts @enter

    until game_is_over(@board) || tie(@board)
      movement(@player_two)

      if !game_is_over(@board) && !tie(@board)
        eval_board(@player_one, @player_two, level)
      end
    end
    puts @end_game
  end


  def computer_to_computer
    board
    puts @enter

    until game_is_over(@board) || tie(@board)
      eval_board(@player_two, @player_one)

      sleep 2

      if !game_is_over(@board) && !tie(@board)
        eval_board(@player_one, @player_two)
      end
    end

    puts @end_game
  end

  def player_to_player
    board
    puts @enter

    until game_is_over(@board) || tie(@board)
      movement(@player_one)

      if !game_is_over(@board) && !tie(@board)
        movement(@player_two)
      end

    end

    puts @end_game
  end

  def movement(player)
    spot = nil

    until spot
      spot = gets.chomp.to_i

      if !@board[spot].include?("X") || !@board[spot].include?("O")
        @board[spot] = player

        board
        return
      else
        spot = nil

        puts "J O G A D A  I N V A L I D A"
      end
    end
  end


  def eval_board(current_player, next_player, level=0)
    spot = nil

    until spot

      if @board[4] == "4" && level != 1
        spot = 4
        @board[spot] = current_player

        board
      else

        spot = get_best_move(@board, current_player, next_player, level)

        if !@board[spot].include?("X") || !@board[spot].include?("O")
          @board[spot] = current_player

          board
        else
          spot = nil
        end
      end
    end
  end

  def get_best_move(board, current_player, next_player, level=0, best_score = {})
    available_spaces = []
    best_move = nil


    board.each do |s|
      unless s.include?("X") || s.include?("O")
        available_spaces << s
      end
    end

    if(level == 1 || level == 2)
      n = rand(0..available_spaces.count)
      return available_spaces[n].to_i
    end

    available_spaces.each_with_index do |as|
      board[as.to_i] = current_player

      if game_is_over(board)

        best_move = as.to_i
        board[as.to_i] = as
        return best_move
      else
        board[as.to_i] = next_player
        if game_is_over(board)

          best_move = as.to_i
          board[as.to_i] = as
          return best_move
        else
          board[as.to_i] = as
        end
      end
    end

    if best_move
      return best_move
    else
      n = rand(0..available_spaces.count)
      return available_spaces[n].to_i
    end
  end


  def game_is_over(b)

    [b[0], b[1], b[2]].uniq.length == 1 ||
    [b[3], b[4], b[5]].uniq.length == 1 ||
    [b[6], b[7], b[8]].uniq.length == 1 ||
    [b[0], b[3], b[6]].uniq.length == 1 ||
    [b[1], b[4], b[7]].uniq.length == 1 ||
    [b[2], b[5], b[8]].uniq.length == 1 ||
    [b[0], b[4], b[8]].uniq.length == 1 ||
    [b[2], b[4], b[6]].uniq.length == 1
  end

  def tie(b)
    b.all? { |s| s.include?("X") || s.include?("O") }
  end

end

game = Game.new

