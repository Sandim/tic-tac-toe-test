class Level

  def levels(game)
    spot = nil
    selected = "S E L E C I O N A D O".green

    puts "S E L E C I O N E  A  D I F I C U D A D E\n"

    puts "1 - Facil".blue
    puts "2 - Medio".magenta
    puts "3 - Dificil".red

    until spot
      spot = gets.chomp.to_i

      if spot == 1
        puts "\nFacil #{selected}"

        game.player_to_computer(1)
      elsif spot == 2
        puts "\nMedio #{selected}"

        game.player_to_computer(2)
      elsif spot == 3
        puts "\nDificil #{selected}"

        game.player_to_computer(3)
      elsif spot == 0
        puts "Até logo :)"

        spot = nil

        return
      else
        puts "Entrada inválida. Use o numérico para selecionar corretamente e aperte tecla enter"

        spot = nil

        menu_players(game)
      end
    end
  end


end
